```sh
naqwada@nslabs:/tmp # id
uid=1000(naqwada) gid=1000(naqwada) groups=1000(naqwada)
naqwada@nslabs:/tmp # ./r00tme
    _   __                                  __         __       
   / | / ___  ____________  ______ ___     / /  ____ _/ /_  _____
  /  |/ / _ \/ ___/ ___/ / / / __ `__ \   / /  / __ `/ __ \/ ___/
 / /|  /  __/ /__/ /  / /_/ / / / / / /  / /__/ /_/ / /_/ /(__  ) 
/_/ |_/\___/\___/_/   \__,_/_/ /_/ /_/  /_____\__,_/_.___//____/  
                                                                                                
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
■ת■!ת■ת■ת■ת■■ת■!ת■ת■ת■ת■qת■ת■ת■תj■ת■ת■תdr■ת■תe■ת■תq■ת■ת!■
Segmentation fault■[]D■■■■■■■■u■■ol╗░iońń

# Sep 21 10:10 kernel: fatal signal: Segmentation fault
kernel died (signal 0, exit 11)

root@nslabs ~ # id
uid=0(root) gid=0(root) groups=0(nslabs)
root@nslabs ~ # cat /root/not-so-secret.txt

           Developer • Security Researcher • Blockchain enthusiast 

❯ 🏡 Website           samy.link/
❯ 📚 Blog              samy.link/blog
❯ 📬 Email             naqwada[`at`]pm.me
❯ 🐦 Twitter           @naqwada_
❯ 📱 Linkdin            linkedin.com/in/samy-younsi/
❯ 🎥 YouTube           youtube.com/channel/UCnPkgUgH05kukpFj8f8UzsA
❯ 🔐 PGP Key           samy.link/docs/pubkey (0x44DB2A2E)
```
---

<div align="center" width="100%">
 <a href="https://samy.link" title="Visit my portfolio!" target="_blank"> <img width="10%" src="https://samy.link/img/naqwada-security-logo.png" alt="Naqwada Security logo"></a>   
  <a href="https://samy.link/blog" title="Visit my blog!" target="_blank"> <img src="https://samy.link/img/logo-geek-large.png" alt="Naqwada Security banner"></a>
</div>
